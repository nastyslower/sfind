# install script to /usr/local/bin for global access

# simpler version of unix find

import argparse
import os
import re
import sys

parser = argparse.ArgumentParser(description='Easily find files and folders.')
parser.add_argument('file', help='File or directory to search for')
parser.add_argument('-d', '--directory', help='Optional starting directory. Defaults to current directory.')
args = parser.parse_args()

def match_filenames():
    wildcard = '\.*'
    pattern = re.compile(wildcard + '\w*\.py', re.IGNORECASE)
    dir = os.getcwd()
    files = os.listdir(dir)
    list = []
    for file in files:
        m = re.search(pattern, file)
        print m
        if (m):
            list.append(os.getcwd() + '/' + file)
            for i in list:
                print "Found: " + i

def main():
    print "running"
    print args.directory
    print os.getcwd()
    # find a file
    match_filenames()

    os.chdir('../')
    print os.getcwd()
    # find another

    os.chdir('sfind')
    print os.getcwd()
    # find another

if __name__ == "__main__":
    print "sfind..."
    print "enter file to find: "
    print "Finding file " + args.file
    print args.directory
    print "finding!"
    main()
