# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary - sfind.py is a simplified version of Unix find. Find files and folders easily!
* Version 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up - clone the repo and cp (you may have to use sudo) sfind.py into /usr/local/bin
* so the script is in your $PATH. You can also edit your ./.bash\_profile and add an alias in order
* to simply type sfind <my-file-name> a la find.
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* richvogtjr@gmail.com
* Other community or team contact: n/a